<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiResponses extends Model
{
    public $typeResponse = null;
    public $message = null;
    public $data = null;
    public $code = null;

    private $failedHttpCode = [400, 404, 422];

    public function makeResponse()
    {
        $response = [
            'status' => [],
        ];

        $response['status'] = $this->typeResponse;
        if ($this->data){
            $response['data'] = $this->data;
        }
        if ($this->message && in_array($this->code, $this->failedHttpCode)){
            $response['error'] = [
                'code' => $this->code,
                'message' => $this->message
            ];
        }

        return $response;
    }

}
