<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFishBreedsLocationsTable extends Migration
{
    private $table = "fish_breeds_locations";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fish_breeds_id')->nullable(false);
            $table->integer('locations_id')->nullable(false);

            $table->index('fish_breeds_id', 'fish_breeds_id_idx_fish_breeds_locations');
            $table->index('locations_id', 'locations_id_idx_fish_breeds_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('fish_breeds_id_idx_fish_breeds_locations');
            $table->dropIndex('locations_id_idx_fish_breeds_locations');
        });

        Schema::dropIfExists($this->table);
    }
}
