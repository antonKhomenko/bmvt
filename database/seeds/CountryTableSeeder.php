<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class CountryTableSeeder extends Seeder
{
    private $table = "countries";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert([
            'country_name' => 'Canada',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_name' => 'USA',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
