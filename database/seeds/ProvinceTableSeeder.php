<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class ProvinceTableSeeder extends Seeder
{
    private $table = "provinces";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = DB::table('countries')
            ->where('country_name', '=', 'Canada')
            ->first();

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Alberta',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'British Columbia',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Quebec',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Manitoba',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Nova Scotia',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Nunavut',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'New Brunswick',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Newfoundland and Labrador',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Ontario',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Prince Edward Island',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Saskatchewan',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Northwest Territories',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'country_id' => $country->id,
            'province_name' => 'Yukon',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
