<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLocations extends Migration
{
    private $table = "locations";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id')->nullable(false);
            $table->integer('country_id')->nullable(false);
            $table->integer('province_id')->nullable(false);
            $table->string('business_name', 255)->nullable(false);
            $table->string('url', 255)->nullable(false);
            $table->string('location_image', 255)->nullable(true);
            $table->integer('use_category_icon')->nullable(true);
            $table->string('marker_image', 255)->nullable(true);
            $table->integer('use_custom_link')->nullable(true);
            $table->string('custom_link', 255)->nullable(true);
            $table->string('language', 100)->nullable(false)->default('English (en-GB)');
            $table->integer('status')->nullable(false)->default(0);
            $table->text('description')->nullable(true);
            $table->string('fish_breeds', 255)->nullable(true);
            $table->string('meta_keywords', 500)->nullable(true);
            $table->string('meta_description', 500)->nullable(true);
            $table->point('location')->nullable(false);
            $table->string('city', 255)->nullable(true);
            $table->string('town', 255)->nullable(true);
            $table->string('postal_code', 8)->nullable(true);
            $table->string('contact_link', 255)->nullable(true);
            $table->string('type_contact_link', 50)->nullable(true);
            $table->string('phone', 50)->nullable(true);
            $table->text('season')->nullable(true);
            $table->timestamps();

            $table->index('category_id', 'category_idx_locations');
            $table->index('business_name', 'business_name_idx_locations');
            $table->index('url', 'url_idx_locations');
            $table->index('status', 'status_idx_locations');
            $table->spatialIndex('location', 'location_idx_locations');
            $table->index('country_id', 'country_idx_location');
            $table->index('province_id', 'province_idx_location');
            $table->unique('business_name', 'business_name_unic_locations');
            $table->unique('url', 'url_unic_locations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('category_idx_locations');
            $table->dropIndex('business_name_idx_locations');
            $table->dropIndex('business_name_unic_locations');
            $table->dropIndex('status_idx_locations');
            $table->dropSpatialIndex('location_idx_locations');
            $table->dropIndex('url_idx_locations');
            $table->dropIndex('url_unic_locations');
            $table->dropIndex('country_idx_location');
            $table->dropIndex('province_idx_location');
        });

        Schema::dropIfExists($this->table);
    }
}
