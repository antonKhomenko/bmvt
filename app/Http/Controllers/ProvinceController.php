<?php

namespace App\Http\Controllers;

use App\ApiResponses;
use App\Provinces;

class ProvinceController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/provinces/{country_id}",
     *     summary="Get province by country id",
     *     tags={"Provinces"},
     *     description="Get province by country id",
     *     @SWG\Parameter(
     *         name="country_id",
     *         in="path",
     *         description="Country Id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Provinces"),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Error input parameter country_id.",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/ProvincesErrorInputData")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Provinces Not Found.",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/ProvincesNotFound")
     *         )
     *     ),
     * )
     */
    public function getProvinceByCountry($country_id)
    {
        $response = new ApiResponses();
        if (!$country_id){
            $response->typeResponse = 'error';
            $response->code = 400;
            $response->message = 'Required parameter not specified.';
            return response()->api($response->makeResponse());
        }

        if (intval($country_id) == 0){
            $response->typeResponse = 'error';
            $response->code = 400;
            $response->message = 'Required parameter must be an integer.';
            return response()->api($response->makeResponse());
        }


        $province = Provinces::getProvinceByCountryId($country_id);

        if (count($province) == 0){
            $response->typeResponse = 'error';
            $response->code = 404;
            $response->message = 'Provinces Not Found.';
        }
        else{
            $response->typeResponse = 'success';
            $response->code = 200;
            $response->data = $province;
        }


        return response()->api($response->makeResponse());
    }
}
