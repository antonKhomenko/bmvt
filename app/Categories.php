<?php

namespace App;

use App\Http\Resources\Category\CategoryCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Categories",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="success"
 *  ),
 *  @SWG\Property(
 *      property="data",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/CategoriesData")
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="CategoriesNotFound",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="error"
 *  ),
 *  @SWG\Property(
 *      property="error",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/CategoriesNotFoundData")
 *  )
 * )
 */
class Categories extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at', 'status'];

    public static function getActiveCategories()
    {
        $categories = Categories::where('status', 1)
            ->orderBy('category_name')
            ->get();

        return new CategoryCollection($categories);
    }
}


/**
 * @SWG\Definition(
 *  definition="CategoriesData",
 *  @SWG\Property(
 *     property="id",
 *     type="integer",
 *     example=3
 *  ),
 *  @SWG\Property(
 *     property="name",
 *     type="string",
 *     example="Accommodations"
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="CategoriesNotFoundData",
 *  @SWG\Property(
 *      property="code",
 *      type="integer",
 *      example=404
 *  ),
 *  @SWG\Property(
 *      property="message",
 *      type="string",
 *      example="Categories Not Found."
 *  )
 * )
 */
