<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFishBreedsTable extends Migration
{
    private $table = "fish_breeds";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fish_breeds_name', 255)->nullable(false);
            $table->string('url', 255)->nullable(false);
            $table->text('description')->nullable(true);
            $table->integer('status')->nullable(false)->default(0);
            $table->timestamps();

            $table->index('fish_breeds_name', 'name_idx_fish_breeds');
            $table->index('status', 'status_idx_fish_breeds');
            $table->index('url', 'url_idx_fish_breeds');
            $table->unique('url', 'url_uniq_cfish_breeds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('name_idx_fish_breeds');
            $table->dropIndex('status_idx_fish_breeds');
            $table->dropIndex('url_idx_fish_breeds');
            $table->dropIndex('url_uniq_cfish_breeds');
        });

        Schema::dropIfExists($this->table);
    }
}
