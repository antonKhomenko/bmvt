<?php

namespace App\Http\Resources\Province;

use App\Http\Resources\Province\ProvinceResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProvinceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function ($province) {
            return new ProvinceResource($province);
        });

        $provinceResource = parent::toArray($request);

        return $provinceResource;
    }
}
