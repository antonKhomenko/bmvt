<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;

class CategoryTableSeeder extends Seeder
{
    private $table = "categories";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert([
            'category_name' => 'Fly-In Camps',
            'url' => Str::slug('Fly-In Camps', '-'),
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Camps Lodges Outfitters',
            'url' => Str::slug('Camps Lodges Outfitters', '-'),
            'description' => 'camps, lodges, outfitters',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Accommodations',
            'url' => Str::slug('Accommodations', '-'),
            'description' => 'cabins, hotels, motels, private rentals',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Camping',
            'url' => Str::slug('Camping', '-'),
            'description' => 'campgrounds and rv-parks',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Fishing Guides',
            'url' => Str::slug('Fishing Guides', '-'),
            'description' => 'fishing guides and services',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Tackle Retailers',
            'url' => Str::slug('Tackle Retailers', '-'),
            'description' => 'tackle stores and retailers',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Events',
            'url' => Str::slug('Events', '-'),
            'description' => 'competitions derbies and events',
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Communities',
            'url' => Str::slug('Communities', '-'),
            'description' => 'towns and communities',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Lakes and Rivers',
            'url' => Str::slug('Lakes and Rivers', '-'),
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Restaurants Gas Stations and More',
            'url' => Str::slug('Restaurants Gas Stations and More', '-'),
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Secret',
            'url' => Str::slug('Secret', '-'),
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table($this->table)->insert([
            'category_name' => 'Demo',
            'url' => Str::slug('Demo', '-'),
            'status' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
