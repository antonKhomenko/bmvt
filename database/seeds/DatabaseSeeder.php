<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(CountryTableSeeder::class);
//        $this->call(ProvinceTableSeeder::class);
//        $this->call(CategoryTableSeeder::class);
//        $this->call(LocationTableSeeder::class);
        $this->call(FishBreedsTableSeeder::class);

    }
}
