<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->get('/locations', 'LocationController@index');

Route::middleware('api')->get('/categories', 'CategoryController@index');

Route::middleware('api')->get('/countries', 'CountryController@index');
Route::middleware('api')->post('/countries', 'CountryController@create');

Route::middleware('api')->get('/provinces/{country_id}', 'ProvinceController@getProvinceByCountry');

Route::middleware('api')->post('/fish-breeds', 'FishBreedsController@create');
