<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProvinces extends Migration
{
    private $table = "provinces";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('country_id')->nullable(false);
            $table->string('province_name', 255)->nullable(false);
            $table->integer('status')->nullable(false)->default(0);
            $table->timestamps();

            $table->index('country_id', 'country_idx_province');
            $table->index('province_name', 'province_name_idx_province');
            $table->index('status', 'status_name_idx_province');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('country_idx_province');
            $table->dropIndex('province_name_idx_province');
            $table->dropIndex('status_name_idx_province');
        });

        Schema::dropIfExists($this->table);
    }
}
