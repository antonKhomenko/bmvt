<?php

namespace App\Http\Resources\Country;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function ($country) {
            return new CountryResource($country);
        });

        $countryResource = parent::toArray($request);

        return $countryResource;
    }
}
