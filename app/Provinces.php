<?php

namespace App;

use App\Http\Resources\Province\ProvinceCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Provinces",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="success"
 *  ),
 *  @SWG\Property(
 *      property="data",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/ProvincesData")
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="ProvincesErrorInput",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="error"
 *  ),
 *  @SWG\Property(
 *      property="error",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/ProvincesErrorInputData")
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="ProvincesNotFound",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="error"
 *  ),
 *  @SWG\Property(
 *      property="error",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/ProvincesNotFoundData")
 *  )
 * )
 */
class Provinces extends Model
{
    protected $table = 'provinces';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at', 'status', 'country_id'];

    public static function getProvinceByCountryId($country_id)
    {
        $provinces = Provinces::where('country_id', $country_id)
            ->orderBy('province_name')
            ->get();

        return new ProvinceCollection($provinces);
    }
}

/**
 * @SWG\Definition(
 *  definition="ProvincesData",
 *  @SWG\Property(
 *      property="id",
 *      type="integer",
 *      example=11
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      example="Saskatchewan"
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="ProvincesErrorInputData",
 *  @SWG\Property(
 *      property="code",
 *      type="integer",
 *      example=400
 *  ),
 *  @SWG\Property(
 *      property="message",
 *      type="string",
 *      example="Error input parameter country_id."
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="ProvincesNotFoundData",
 *  @SWG\Property(
 *      property="code",
 *      type="integer",
 *      example=404
 *  ),
 *  @SWG\Property(
 *      property="message",
 *      type="string",
 *      example="Provinces Not Found."
 *  )
 * )
 */
