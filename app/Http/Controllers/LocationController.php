<?php

namespace App\Http\Controllers;

use App\ApiResponses;
use App\Locations;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/locations",
     *     summary="Get list Location",
     *     tags={"Location"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Locations")
     *         ),
     *     )
     * )
     */
    public function index(Request $request)
    {
        if ($request){
            $locations = new Locations();
            $data = $locations->getLocationByParams($request);
        }
        else{
            $data = Locations::getLocations();
        }


        $response = new ApiResponses();

        if (count($data) == 0){
            $response->typeResponse = 'error';
            $response->code = 404;
            $response->message = 'Locations Not Found.';
        }
        else{
            $response->typeResponse = 'success';
            $response->code = 200;
            $response->data = $data;
        }


        return response()->api($response->makeResponse());
    }
}
