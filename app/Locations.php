<?php

namespace App;

use App\Http\Resources\Location\LocationCollection;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @SWG\Definition(
 *  definition="Locations",
 *  @SWG\Property (property="id", type="integer", example=1),
 *  @SWG\Property (property="name", type="string", example="Admiral Reservoir"),
 *  @SWG\Property (property="url", type="string", example="admiral-reservoir"),
 *  @SWG\Property (property="locationImage", type="string"),
 *  @SWG\Property (property="markerImage", type="string"),
 *  @SWG\Property (property="description", type="string", example="<p>2009 - <b>Walleye</b> Fry 100000 2009 - <b>Yellow Perch</b> Catchables 1275 2011</p>"),
 *  @SWG\Property (property="fishBreed", type="string"),
 *  @SWG\Property (property="metaKeywords", type="string"),
 *  @SWG\Property (property="metaDescription", type="string"),
 *  @SWG\Property (property="lat", type="number", format="float", example=40.548512, description="latitude (широта)"),
 *  @SWG\Property (property="lon", type="number", format="float", example=-105.548512, description="longitude (долгота)"),
 *  @SWG\Property (property="city", type="string"),
 *  @SWG\Property (property="town", type="string"),
 *  @SWG\Property (property="postalCode", type="string"),
 *  @SWG\Property (property="contactLink", type="string"),
 *  @SWG\Property (property="typeContactLink", type="string"),
 *  @SWG\Property (property="phone", type="string"),
 *  @SWG\Property (property="season", type="string"),
 *  @SWG\Property (property="country", type="array", @SWG\Items(ref="#/definitions/CountriesData")),
 *  @SWG\Property (property="province", type="array", @SWG\Items(ref="#/definitions/ProvincesData")),
 *  @SWG\Property (property="category", type="array", @SWG\Items(ref="#/definitions/CategoriesData")),
 * )
 */
class Locations extends Model
{
    use SpatialTrait;

    protected $table = 'locations';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at', 'status'];

    protected $spatialFields = [
        'location'
    ];

    public function country()
    {
        return $this->hasOne('App\Countries', 'id', 'country_id');
    }

    public function province()
    {
        return $this->hasOne('App\Provinces', 'id', 'province_id');
    }

    public function category()
    {
        return $this->hasOne('App\Categories', 'id', 'category_id');
    }

    public function fish_breeds()
    {
        return $this->belongsToMany('App\FishBreeds');
    }

    public static function getLocations()
    {
        $locations = Locations::where('status', 1)
            ->orderBy('business_name')
            ->get();

        return new LocationCollection($locations);
    }

    public function getLocationByParams(Request $request)
    {

        $query = Locations::query();

        $p = new Locations();
        $query = $p->newQuery();

        if ($request->get('country_id')){
            $query->where('country_id', request('country_id'));

        }

        if ($request->get('province_id')){
            $query->where('province_id', request('province'));
        }

        if ($request->get('fish')){
            $query->whereHas('fish_breeds', function ($query) use ($request){

                $queryArr = explode(",", request('fish'));
                foreach ($queryArr AS $k=>$v){
                    $queryArr[$k] = intval($v);
                }

                $query->whereIn('fish_breeds.id', $queryArr);
            });
        }

        return new LocationCollection($query->get()->all());

    }
}
