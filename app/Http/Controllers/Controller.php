<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host="getfishing.loc",
 *   basePath="/",
 *   @SWG\Info(
 *     title="Get Fishing API",
 *     version="1.0.0",
 *     description="This document describes the API for the interaction between the client and the server to build an application that provides the site’s getfishing.online",
 *     contact={
 *       "email": "xoma99@mail.ru"
 *     }
 *   )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
