<?php

namespace App\Http\Controllers;

use App\ApiResponses;
use App\Countries;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/countries",
     *     summary="Get list Country",
     *     description="The method returns a list of countries for objects on the map.",
     *     tags={"Countries"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Countries")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Countries Not Found.",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CountriesNotFound")
     *         )
     *     ),
     * )
     */
    public function index ()
    {
        $countries = Countries::getActiveCountries();

        $response = new ApiResponses();

        if (count($countries) == 0){
            $response->typeResponse = 'error';
            $response->code = 404;
            $response->message = 'Countries Not Found.';
        }
        else{
            $response->typeResponse = 'success';
            $response->code = 200;
            $response->data = $countries;
        }

        return response()->api($response->makeResponse());
    }

    /**
     * Store a new blog post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:countries,country_name|max:255'
        ]);

        $response = new ApiResponses();

        if ($validator->fails()) {
            $response->typeResponse = 'error';
            $response->code = 422;
            $response->message = $validator->messages()->first();
            return response()->api($response->makeResponse());
        }

        $response->code = 201;
        $response->typeResponse = 'success';
        $response->message = 'Creation was successful.';
        return response()->api($response->makeResponse());
    }
}
