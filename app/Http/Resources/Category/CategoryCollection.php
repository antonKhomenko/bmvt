<?php

namespace App\Http\Resources\Category;

use App\Http\ResourcesCategory\CategoryResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function ($category) {
            return new CategoryResource($category);
        });

        $categoryResource = parent::toArray($request);

        return $categoryResource;
    }
}
