<?php

namespace App\Http\Controllers;


use App\ApiResponses;
use App\Categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/categories",
     *     summary="Get list Category",
     *     description="The method returns a list of categories for objects on the map.",
     *     tags={"Categories"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Categories")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Categories Not Found.",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CategoriesNotFound")
     *         )
     *     ),
     * )
     */
    public function index ()
    {
        $category = Categories::getActiveCategories();

        $response = new ApiResponses();

        if (count($category) == 0){
            $response->typeResponse = 'error';
            $response->code = 404;
            $response->message = 'Categories Not Found.';
        }
        else{
            $response->typeResponse = 'success';
            $response->code = 200;
            $response->data = $category;
        }

        return response()->api($response->makeResponse());
    }
}
