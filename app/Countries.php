<?php

namespace App;

use App\Http\Resources\Country\CountryCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Countries",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="success"
 *  ),
 *  @SWG\Property(
 *      property="data",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/CountriesData")
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="CountriesNotFound",
 *  @SWG\Property(
 *      property="status",
 *      type="string",
 *      example="error"
 *  ),
 *  @SWG\Property(
 *      property="error",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/CountriesNotFoundData")
 *  )
 * )
 */
class Countries extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at', 'status'];

    public static function getActiveCountries()
    {
        $countries = Countries::where('status', 1)
            ->orderBy('country_name')
            ->get();

        return new CountryCollection($countries);
    }
}

/**
 * @SWG\Definition(
 *  definition="CountriesData",
 *  @SWG\Property(
 *      property="id",
 *      type="integer",
 *      example=1
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      example="Canada"
 *  )
 * )
 *
 * @SWG\Definition(
 *  definition="CountriesNotFoundData",
 *  @SWG\Property(
 *      property="code",
 *      type="integer",
 *      example=404
 *  ),
 *  @SWG\Property(
 *      property="message",
 *      type="string",
 *      example="Countries Not Found."
 *  )
 * )
 */
