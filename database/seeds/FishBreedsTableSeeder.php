<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Str;

class FishBreedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_decode('[{"name": "Northern Pike"}, {"name": "Walleye"}, {"name": "Yellow Perch"}, {"name": "Brown Trout"}, {"name": "Brook Trout"}, {"name": "Burbot"}]');

        foreach ($data AS $d){
            $location = new \App\FishBreeds();
            $location->fish_breeds_name = $d->name;
            $location->url = Str::slug($d->name, '-');
            $location->created_at = now();
            $location->updated_at = now();
            $location->save();
        }
    }
}
