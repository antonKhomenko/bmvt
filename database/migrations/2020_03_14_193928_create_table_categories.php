<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCategories extends Migration
{
    private $table = "categories";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_name', 255)->nullable(false);
            $table->string('url', 255)->nullable(false);
            $table->string('marker_image', 255)->nullable(true);
            $table->integer('status')->nullable(false)->default(0);
            $table->text('description')->nullable(true);
            $table->string('tags', 255)->nullable(true);
            $table->string('meta_keywords', 255)->nullable(true);
            $table->string('meta_description', 255)->nullable(true);
            $table->timestamps();


            $table->index('category_name', 'category_name_idx_categories');
            $table->index('tags', 'tags_idx_categories');
            $table->index('status', 'status_idx_categories');
            $table->unique('url', 'url_idx_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('category_name_idx_categories');
            $table->dropIndex('tags_idx_categories');
            $table->dropIndex('status_idx_categories');
            $table->dropIndex('url_idx_categories');
        });

        Schema::dropIfExists($this->table);
    }
}
