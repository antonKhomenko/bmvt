<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class FishBreeds extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'fish_breeds';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];

    public static function getAllBreeds()
    {
        return FishBreeds::all();
    }

    public static function store(Request $request)
    {
        $result = false;

        $fb = new FishBreeds();
        $fb->fish_breeds_name = $request->get('fishBreedsName');
        $fb->url = Str::slug($request->get('fishBreedsName'), '-');
        $fb->description = $request->get('fishBreedsDescription');
        $fb->status = 1;
        if ($fb->save()){
            $id = $fb->id;
            $fish = FishBreeds::find($id);
            $fish->addMediaFromRequest('image')->toMediaCollection('images');
            $result = true;
        }

        return $result;
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->height(150)
            ->quality(100)
            ->sharpen(5);
    }
}
