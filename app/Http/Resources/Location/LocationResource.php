<?php

namespace App\Http\Resources\Location;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Province\ProvinceResource;
use App\Http\ResourcesCategory\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->business_name,
            'url' => $this->url,
            'locationImage' => $this->location_image,
            'markerImage' => $this->marker_image,
            'description' => $this->description,
            'fishBreed' => $this->fish_breeds,
            'metaKeywords' => $this->meta_keywords,
            'metaDescription' => $this->meta_description,
            'lat' => $this->location->getLat(),
            'lon' => $this->location->getLng(),
            'city' => $this->city,
            'town' => $this->town,
            'postalCode' => $this->postal_code,
            'contactLink' => $this->contact_link,
            'typeContactLink' => $this->type_contact_link,
            'phone' => $this->phone,
            'season' => $this->season,
            'country' => new CountryResource($this->country),
            'province' => new ProvinceResource($this->province),
            'category' => new CategoryResource($this->category)
        ];
    }
}
