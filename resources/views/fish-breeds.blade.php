<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link media="all" rel="stylesheet" href="/bootstrap/css/bootstrap.css">

        <script type="text/javascript" src="jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>



        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 97vh;
                margin: 1vh 10px;

            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <section>
                <div class="row">
                    <div class="col-3">
                        <h4>Fish Breeds Ref</h4>
                        <form id="fish-breeds">
                            <input type="text" id="fish_breeds_name" value="" placeholder="fish breeds name"/>
                            <textarea id="fish_breeds_description"></textarea>

                            <input type="file" multiple="multiple" accept=".txt,image/*">

                            <div class="ajax-reply"></div>

                            <input type="button" value="Save" id="save_fish_breed">
                        </form>
                    </div>
                    <div class="col-3">
                        <h4>Countries Ref</h4>
                    </div>
                </div>
        </section>


    <script>

        var files; // переменная. будет содержать данные файлов

        // заполняем переменную данными, при изменении значения поля file
        $('input[type=file]').on('change', function(){
            files = this.files;
        });

        $('#save_fish_breed').on( 'click', function( event ){

            event.stopPropagation(); // остановка всех текущих JS событий
            event.preventDefault();  // остановка дефолтного события для текущего элемента - клик для <a> тега

            // ничего не делаем если files пустой
            // if( typeof files == 'undefined' ) return;

            // создадим объект данных формы
            var data = new FormData();

            // заполняем объект данных файлами в подходящем для отправки формате
            $.each( files, function( key, value ){
                data.append( 'image', value );
            });

            // добавим переменную для идентификации запроса
            data.append( 'my_file_upload', 1 );

            data.append('fishBreedsName', $("#fish_breeds_name").val());
            data.append('fishBreedsDescription', $("#fish_breeds_description").val());

            // AJAX запрос
            $.ajax({
                url         : '/api/fish-breeds',
                type        : 'POST', // важно!
                data        : data,
                cache       : false,
                dataType    : 'json',
                // отключаем обработку передаваемых данных, пусть передаются как есть
                processData : false,
                // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
                contentType : false,
                // функция успешного ответа сервера
                success     : function( respond, status, jqXHR ){

                    // ОК - файлы загружены
                    if( typeof respond.error === 'undefined' ){
                        // выведем пути загруженных файлов в блок '.ajax-reply'
                        var files_path = respond.files;
                        var html = '';
                        $.each( files_path, function( key, val ){
                            html += val +'<br>';
                        } )

                        $('.ajax-reply').html( html );
                    }
                    // ошибка
                    else {
                        console.log('ОШИБКА: ' + respond.error );
                    }
                },
                // функция ошибки ответа сервера
                error: function( jqXHR, status, errorThrown ){
                    console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
                }

            });

        });


        function save(){
            var fish_breeds_name = $("#fish_breeds_name").val();
            var fish_breeds_description = $("#fish_breeds_description").val();

            $.ajax({
                url: "/api/fish-breeds",
                data: {
                    fishBreedsName: fish_breeds_name,
                    fishBreedsDescription: fish_breeds_description,
                },
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    // $('.category-details').html(data);
                    console.log(data)
                }
            });
        }

    </script>

    </body>
</html>
