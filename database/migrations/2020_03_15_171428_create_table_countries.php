<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCountries extends Migration
{
    private $table = "countries";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country_name', 255)->nullable(false);
            $table->integer('status')->nullable(false)->default(0);
            $table->timestamps();

            $table->index('country_name', 'country_name_idx_countries');
            $table->index('status', 'status_name_idx_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropIndex('country_name_idx_countries');
            $table->dropIndex('status_name_idx_countries');
        });

        Schema::dropIfExists($this->table);
    }
}
