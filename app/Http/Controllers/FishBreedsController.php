<?php

namespace App\Http\Controllers;

use App\ApiResponses;
use App\FishBreeds;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FishBreedsController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fishBreedsName' => 'required|unique:fish_breeds,fish_breeds_name|max:255'
        ]);

        $response = new ApiResponses();

        if ($validator->fails()) {
            $response->typeResponse = 'error';
            $response->code = 422;
            $response->message = $validator->messages()->first();
            return response()->api($response->makeResponse());
        }

        if (FishBreeds::store($request)){
            $response->code = 201;
            $response->typeResponse = 'success';
            $response->message = 'Creation was successful.';
        }
        else{
            $response->code = 500;
            $response->typeResponse = 'error';
            $response->message = 'Some server error.';
        }


        return response()->api($response->makeResponse());
    }
}
